import json
import aqi

from django.shortcuts import render


def get_color(aqi):
    if aqi >= 50 and aqi < 100:
        color = "yellow"
    elif aqi >= 100 and aqi < 150:
        color = "orange"
    elif aqi >= 150 and aqi < 200:
        color = "red"
    elif aqi >= 200 and aqi < 300:
        color = "purple"
    elif aqi >= 300:
        color = "brown"
    else:
        color = "Lime"

    text = "white"
    if aqi > 200:
        text = "black"

    return {"bg": color, "text": text}


def index(request):
    with open("/var/www/html/aqi.json") as json_file:
        data = json.load(json_file)
        sensor_data = data[-1]

    sensor_data["aqiPm25"] = aqi.to_iaqi(aqi.POLLUTANT_PM25,
            sensor_data["pm25"], algo=aqi.ALGO_EPA)
    sensor_data["pm25_colour"] = get_color(sensor_data["aqiPm25"])
    sensor_data["aqiPm10"] = aqi.to_iaqi(aqi.POLLUTANT_PM10,
            sensor_data["pm10"], algo=aqi.ALGO_EPA)
    sensor_data["pm10_colour"] = get_color(sensor_data["aqiPm10"])

    context = {'sensor_data': sensor_data}
    return render(request, 'sensor/index.html', context)

# django aqi

This is a educational project to learn basics of django. Idea is based on blog post [How To Measure Particulate Matter With a Raspberry Pi](https://hackernoon.com/how-to-measure-particulate-matter-with-a-raspberry-pi-75faa470ec35).
Parts of the code are based on orginal [zefanja's aqi](https://github.com/zefanja/aqi) project. I have tested it with sensor SDS021 but there should be no difference for SDS011.

---

## Setup

To setup project:

1. First follow [blog post](https://hackernoon.com/how-to-measure-particulate-matter-with-a-raspberry-pi-75faa470ec35) instructions to obtain and run aqi.py script.
2. Then clone this project '''git clone https://bitbucket.org/karulis/django_aqi.git'''(it is based on first three steps of [django tutorial](https://docs.djangoproject.com/en/2.1/intro/tutorial01/)).
3. Now You should be able to run "python3 manage.py runserver 0:8888" and see page http://localhost:8888/.

---

## Next steps

Some bonus steps:

1. To deploy project with with uWSGI and nginx try [this tutorial](https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html)(files in [deploy](https://bitbucket.org/karulis/django_aqi/src/master/deploy/) will be usefull). 
Step "Make uWSGI startup when the system boots" did not work for me, I had to follow [systemd instructions](https://uwsgi-docs.readthedocs.io/en/latest/Systemd.html) for uWSGI.
2. If You would like to access sensor measurments from anywhere in the world, You may try [tor hidden service](https://www.torproject.org/docs/tor-onion-service.html) setup.

---

## CAQI, voice alert and Bokeh plot

Above setup is for AQI index, in EU CAQI is more often used, to make use of it try: [caqi_branch](https://bitbucket.org/karulis/django_aqi/src/caqi_and_plot/)
